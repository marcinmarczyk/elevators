FROM openjdk:8-jre-alpine

COPY target/elevator-1.0-SNAPSHOT.jar /app.jar

CMD ["/usr/bin/java", "-jar", "-Dspring.profiles.active=prod", "/app.jar"]
