## Description of the implementation

The basic solution to elevator system has been implemented:
 * every elevator is a daemon thread managed by _ThreadExecutor_
 * controller sends two events to _EventBus_ (`RequestElevatorEvent`, `SendElevatorWithUserEvent`), which are handled by elevators
 * tests were implemented: 
   * using _Groovy_ and _Spock Framework_ (unit and http requests tests)
   * using _JUnit_ and _Awaitility_ (events handling)
 * code coverage handled by _Jacoco_

## How to configure the program
The application has some configurable properties, which can be found in `application.properties` file:
 * `com.tingco.elevator.numberofelevators` - number of elevators
 * `com.tingco.elevator.numberoffloors` - number of floors
 * `com.tingco.elevator.secondswaiting` - how many seconds should pass before elevator changes state from `WAITING_FOR_FLOOR_CHOICE` to `IDLE`

## Description of the solution

Program has some rules worth mentioning:
 * at the program start all elevators are parked at floor 0
 * elevator has direction: `NONE`, `DOWN`, `UP`, this is being set based on `addressedFloor` and `currentFloor` parameters
 * elevator has states: `IDLE`, `MOVING`, `MOVING_WITH_USER`, `WAITING_FOR_FLOOR_CHOICE` (described later)
 * when user calls the elevator to specific floor, controller chooses suitable one (the one that is idle and is the closest to the user), that's the moment when elevator changes state from `IDLE` to `MOVING`
 * if there are no idle elevators, than the controller adds the request to queue
 * controller checks if there are any idle elevators and sends it to requested floor every second
 * when elevator reaches requested floor, then it changes state to `WAITING_FOR_FLOOR_CHOICE`
 * if user chose destination floor, than the elevator changes state to `MOVING_WITH_USER` and starts to move to destination floor
 * if user didn't choose the destination floor for parametrized amount of time (`com.tingco.elevator.secondswaiting`), than the elevator changes state to `IDLE`
 * if elevator is in state `MOVING_WITH_USER` and have multiple destination floor buttons pressed (there are more users in the same elevator and they have chosen different destination floors), then the algorithm sorts floors and handle the requests optimum way
   * e.g.: 
     * elevator is on the 8th floor
     * first user presses 0 floor 
     * second user presses 5th floor button
     * elevator first goes to 5th floor and then to 0
   * another more complicated example:
     * elevator is on the 8th floor
     * first user presses 0 floor button (elevator starts moving to that floor, elevators direction is set to `DOWN`)
     * second user presses 5th floor
     * third user presses 9th floor
     * the elevator will handle requests like this: 5, 0, 9 (floors)
 * after last request handled, the elevator goes back to `IDLE` state

### Elevator states

Below is the description of every elevator state.

 * `IDLE` - elevator is not moving and is not busy
 * `MOVING` - elevator is moving, has been called by user to specific floor (using button outside of the elevator)
 * `WAITING_FOR_FLOOR_CHOICE` - elevator is parked on specific floor and waiting for user to choose destination floor (using button inside the elevator)
 * `MOVING_WITH_USER` - user has chosen destination floor and the elevator is moving toward that floor

## What to implement next

 * sorting algorithm for calling elevator requests (buttons pressed outside of the elevator)
 * performance tests

## Build And Run

As the project is, the Spring app can be started as seen below.

build and run the code with Maven

    mvn package
    mvn spring-boot:run

or start the target JAR file 

    mvn package
    java -jar target/elevator-1.0-SNAPSHOT.jar

Project can be run using Docker and docker-compose
    
    docker-compose up

Docker image is configured to use 10 floors and 2 elevators. 
