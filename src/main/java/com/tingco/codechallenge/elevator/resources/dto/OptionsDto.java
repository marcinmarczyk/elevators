package com.tingco.codechallenge.elevator.resources.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Transport object containing application options.
 */
@Data
@Builder
public class OptionsDto {
    private int numberOfElevators;
    private int numberOfFloors;
}
