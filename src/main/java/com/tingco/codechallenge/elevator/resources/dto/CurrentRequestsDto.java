package com.tingco.codechallenge.elevator.resources.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Transport object containing list of current requests.
 */
@Data
@Builder
public class CurrentRequestsDto {
    private List<Integer> currentRequests;
}
