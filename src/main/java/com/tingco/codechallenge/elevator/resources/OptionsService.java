package com.tingco.codechallenge.elevator.resources;

import com.tingco.codechallenge.elevator.resources.dto.OptionsDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Service returning application options.
 */
@Service
public class OptionsService {

    @Value("${com.tingco.elevator.numberofelevators}")
    private Integer numberOfElevators;

    @Value("${com.tingco.elevator.numberoffloors}")
    private Integer numberOfFloors;

    OptionsDto getOptions() {
        return OptionsDto.builder()
                .numberOfElevators(numberOfElevators)
                .numberOfFloors(numberOfFloors)
                .build();
    }
}
