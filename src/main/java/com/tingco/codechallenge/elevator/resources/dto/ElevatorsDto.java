package com.tingco.codechallenge.elevator.resources.dto;

import com.tingco.codechallenge.elevator.api.Elevator;
import com.tingco.codechallenge.elevator.api.State;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Transport object containing list of elevators.
 */
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ElevatorsDto {

    private List<ElevatorDto> elevatorDtos;

    public static ElevatorsDto of(List<Elevator> elevators){
        List<ElevatorDto> elevatorDtos = elevators.stream().map(elevator ->
                ElevatorDto.builder()
                        .id(elevator.getId())
                        .floor(elevator.currentFloor())
                        .isMoving(elevator.isBusy())
                        .state(elevator.getState())
                        .direction(elevator.getDirection())
                        .build()
            ).collect(Collectors.toList());
        return new ElevatorsDto(elevatorDtos);
    }

    @Data
    @Builder
    static class ElevatorDto {
        Integer id;
        Integer floor;
        boolean isMoving;
        State state;
        Elevator.Direction direction;
    }
}
