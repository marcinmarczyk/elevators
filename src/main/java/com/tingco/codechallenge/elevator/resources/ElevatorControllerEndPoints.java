package com.tingco.codechallenge.elevator.resources;

import com.google.common.collect.Lists;
import com.tingco.codechallenge.elevator.api.Elevator;
import com.tingco.codechallenge.elevator.api.ElevatorController;
import com.tingco.codechallenge.elevator.api.ElevatorImpl;
import com.tingco.codechallenge.elevator.exception.ElevatorNotFoundException;
import com.tingco.codechallenge.elevator.resources.dto.CurrentRequestsDto;
import com.tingco.codechallenge.elevator.resources.dto.ElevatorsDto;
import com.tingco.codechallenge.elevator.resources.dto.OptionsDto;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest Resource.
 *
 * @author Sven Wesley
 */
@RestController
@RequestMapping("/rest/v1")
@AllArgsConstructor
@CrossOrigin
public final class ElevatorControllerEndPoints {

    private final ElevatorController elevatorController;
    private final OptionsService optionsService;

    /**
     * Requests elevator to specified floor.
     * @param floorNumber
     *          Destination floor number.
     * @return {@link StatusDto} of the request.
     */
    @GetMapping("/requestElevator/{floorNumber}")
    public ResponseEntity<StatusDto> elevatorRequested(@PathVariable int floorNumber) {
        elevatorController.requestElevator(floorNumber);
        return ResponseEntity.ok(new StatusDto(Status.OK));
    }

    /**
     * Endpoint for choosing floor.
     * @param elevatorId
     *          Id of the elevator in which event occurred.
     * @param floorNumber
     *          Number of floor chosen.
     * @return {@link StatusDto} of the request.
     */
    @GetMapping("/chooseFloor/{elevatorId}/{floorNumber}")
    public ResponseEntity<StatusDto> chooseFloor(@PathVariable int elevatorId, @PathVariable int floorNumber) {
        elevatorController.chooseFloor(elevatorId, floorNumber);
        return ResponseEntity.ok(new StatusDto(Status.OK));
    }

    /**
     * Get application options (number of elevators and floors).
     * @return {@link OptionsDto}
     */
    @GetMapping("/options")
    public ResponseEntity<OptionsDto> getOptions() {
        return ResponseEntity.ok(optionsService.getOptions());
    }

    /**
     * Get elevators with current state.
     * @return {@link ElevatorsDto}
     */
    @GetMapping("/currentElevators")
    public ResponseEntity<ElevatorsDto> getCurrentPositionOfElevators() {
        return ResponseEntity.ok(ElevatorsDto.of(elevatorController.getElevators()));
    }

    /**
     * Get current elevator requests.
     * @return {@link CurrentRequestsDto}
     */
    @GetMapping("/currentRequests")
    public ResponseEntity<CurrentRequestsDto> getCurrentRequests() {
     return ResponseEntity.ok(CurrentRequestsDto.builder().currentRequests(elevatorController.getFloorRequests()).build());
    }

    /**
     * Ping service to test if we are alive.
     *
     * @return String pong
     */
    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public String ping() {

        return "pong";
    }

    @AllArgsConstructor
    @Value
    private class StatusDto {
        private Status status;
    }

    enum Status {
        OK,
    }
}
