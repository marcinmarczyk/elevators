package com.tingco.codechallenge.elevator.event;

import lombok.Builder;
import lombok.Value;

/**
 * Event representing user elevator request on floor.
 */
@Value
@Builder
public class RequestElevatorEvent {
    private final int floorNum;
    private final int elevatorId;
}
