package com.tingco.codechallenge.elevator.event;

import lombok.Builder;
import lombok.Value;

/**
 * Event sent by controller to send elevator to specified floor.
 */
@Value
@Builder
public class SendElevatorWithUserEvent {
    private final int floorNum;
    private final int elevatorId;
}
