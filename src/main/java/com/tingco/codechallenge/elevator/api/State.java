package com.tingco.codechallenge.elevator.api;

/**
 * Represents state in which is the elevator.
 */
public enum State {
    IDLE,
    WAITING_FOR_FLOOR_CHOICE,
    MOVING,
    MOVING_WITH_USER
}
