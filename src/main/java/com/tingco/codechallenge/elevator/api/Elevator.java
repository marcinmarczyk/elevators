package com.tingco.codechallenge.elevator.api;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Interface for an elevator object.
 *
 * @author Sven Wesley
 */
public interface Elevator {

    /**
     * Enumeration for describing elevator's direction.
     */
    enum Direction {
        UP, DOWN, NONE
    }

    /**
     * Tells which direction is the elevator going in.
     *
     * @return Direction Enumeration value describing the direction.
     */
    Direction getDirection();

    /**
     * If the elevator is moving. This is the target floor.
     *
     * @return primitive integer number of floor
     */
    int getAddressedFloor();

    /**
     * Get the Id of this elevator.
     *
     * @return primitive integer representing the elevator.
     */
    int getId();

    /**
     * Command to move the elevator to the given floor.
     *
     * @param toFloor int where to go.
     */
    void moveElevator(int toFloor);

    /**
     * Check if the elevator is occupied at the moment.
     *
     * @return true if busy.
     */
    boolean isBusy();

    /**
     * Reports which floor the elevator is at right now.
     *
     * @return int actual floor at the moment.
     */
    int currentFloor();

    /**
     * Move elevator one floor.
     */
    void moveOneFloor();

    /**
     * Get state of the elevator.
     *
     * @return {@link State} state of the elevator.
     */
    State getState();

    /**
     * Sets {@link State} of the elevator as IDLE.
     */
    void idle();

    /**
     * Sets {@link State} of the elevator as WAITING_FOR_USER_CHOICE.
     */
    void waitingForUserChoice();

    /**
     * Sets {@link State} of the elevator as MOVING.
     */
    void moving();

    /**
     * Sets {@link State} of the elevator as MOVING_WITH_USER.
     */
    void movingWithUser();

    /**
     * Reports time from which elevator waits for user set the destination floor.
     *
     * @return {@link LocalDateTime} timestamp from when elevator is in WAITING_FOR_FLOOR_CHOICE state.
     */
    LocalDateTime getWaitingTimestamp();
}
