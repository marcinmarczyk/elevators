package com.tingco.codechallenge.elevator.api;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DeadEventHandler {

    @Subscribe
    public void deadEvent(DeadEvent deadEvent) {
        log.error("Registerred event with no subscribers: {}", deadEvent.toString());
    }

}
