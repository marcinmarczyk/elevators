package com.tingco.codechallenge.elevator.api;

import io.vavr.control.Try;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class ElevatorThread extends Thread {

    private Elevator elevator;

    ElevatorThread(Elevator elevator) {
        this.setDaemon(true);
        this.elevator = elevator;
    }

    @Override
    public void run() {
        super.run();
        log.info("Running thread for elevator with id: {}", elevator.getId());
        while(true) {
            Try.run(()->Thread.sleep(1000));
            elevator.moveOneFloor();
        }
    }
}
