package com.tingco.codechallenge.elevator.api;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.tingco.codechallenge.elevator.event.RequestElevatorEvent;
import com.tingco.codechallenge.elevator.event.SendElevatorWithUserEvent;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.tingco.codechallenge.elevator.api.State.*;

@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@AllArgsConstructor
@Slf4j
public class ElevatorImpl implements Elevator {

    @Getter
    private final int id;

    @Getter
    private Direction direction;

    private int currentFloor;

    private int addressedFloor;

    @Getter
    private List<Integer> activatedButtons = Lists.newArrayList();

    private State state;

    @Getter
    private LocalDateTime waitingTimestamp;

    private ElevatorImpl(int id) {
        checkNotNull(id);

        this.id = id;
        this.direction = Direction.NONE;
        idle();
    }

    static Elevator of(int id) {
        return new ElevatorImpl(id);
    }

    static Elevator of(ElevatorThread elevatorThread) {
        checkNotNull(elevatorThread);

        return elevatorThread.getElevator();
    }

    @Subscribe
    public void getEvent(RequestElevatorEvent event) {
        log.info("Received RequestElevatorEvent {}", event);
        if (event.getElevatorId() == id) {
            moving();
            moveElevator(event.getFloorNum());
        }
    }

    @Subscribe
    public void getEvent(SendElevatorWithUserEvent event) {
        log.info("Received SendElevatorWithUserEvent {}", event);
        if (event.getElevatorId() == id && (this.state == WAITING_FOR_FLOOR_CHOICE || this.state == IDLE || this.state == MOVING_WITH_USER) ) {
            movingWithUser();
            moveElevator(event.getFloorNum());
        }
    }

    @Override
    public void moveElevator(int toFloor) {
        log.info("Elevator(id={}) going to floor {} from {}", id, toFloor, currentFloor);
        activatedButtons.add(toFloor);
    }

    @Override
    public void moveOneFloor(){
        if (!isBusy() && !activatedButtons.isEmpty()) {
            addressedFloor = activatedButtons.remove(0);
        }

        specifyDirection();
        sortActivatedButtons();

        if (direction == Direction.DOWN){
            currentFloor--;
            log.info("Moving elevator(id={}) to floor {}, current {} ({})", id, getAddressedFloor(), currentFloor, this);
        } else if(direction == Direction.UP) {
            currentFloor++;
            log.info("Moving elevator(id={}) to floor {}, current {} ({})", id, getAddressedFloor(), currentFloor, this);
        } else if(direction == Direction.NONE) {
            if (this.state == MOVING){
                if (activatedButtons.isEmpty()) {
                    waitingForUserChoice();
                }
            } else if(this.state == MOVING_WITH_USER) {
                idle();
            }
            log.info("Elevator(id={}) is not moving ({})", id, this);
        }
    }

    private void specifyDirection() {
        if (addressedFloor < currentFloor){
            direction = Direction.DOWN;
        } else if (addressedFloor > currentFloor) {
            direction = Direction.UP;
        } else {
            direction = Direction.NONE;
        }
    }

    private List<Integer> addAddressedFloorToSortOperation(List<Integer> activatedButtons, State state, int addressedFloor){
        List<Integer> activatedToSort = new ArrayList<>(activatedButtons);
        if (state == MOVING_WITH_USER && !activatedToSort.contains(addressedFloor)){
            activatedToSort.add(addressedFloor);
        }
        return activatedToSort;
    }

    private void sortActivatedButtons() {
        this.activatedButtons = addAddressedFloorToSortOperation(activatedButtons, state, addressedFloor);
        List<Integer> toEnd = Lists.newLinkedList();
        List<Integer> toSort = Lists.newLinkedList();

        if (direction == Direction.DOWN) {
            toSort = activatedButtons.stream().filter(i -> i < currentFloor).collect(Collectors.toList());
            toEnd = activatedButtons.stream().filter(i -> i >= currentFloor).collect(Collectors.toList());
        } else if (direction == Direction.UP) {
            toSort = activatedButtons.stream().filter(i -> i > currentFloor).collect(Collectors.toList());
            toEnd = activatedButtons.stream().filter(i -> i <= currentFloor).collect(Collectors.toList());
        }

        toSort.sort(Comparator.naturalOrder());
        toEnd.sort(Comparator.naturalOrder());

        if (direction == Direction.DOWN) {
            toSort.sort(Comparator.reverseOrder());
        } else {
            toEnd.sort(Comparator.reverseOrder());
        }
        activatedButtons.clear();
        activatedButtons.addAll(toSort);
        activatedButtons.addAll(toEnd);
        setNewAddressedFloor(this.state, this.activatedButtons);
        activatedButtons.remove(new Integer(addressedFloor));
    }

    private void setNewAddressedFloor(State state, List<Integer> activatedButtons) {
        if (state == MOVING_WITH_USER && !activatedButtons.isEmpty()){
            addressedFloor = activatedButtons.remove(0);
        }
    }

    @Override
    public boolean isBusy() {
        return addressedFloor != currentFloor;
    }

    @Override
    public int currentFloor() {
        return currentFloor;
    }

    @Override
    public int getAddressedFloor() {
        return addressedFloor;
    }

    @Override
    public State getState() {
        return state;
    }

    public void idle(){
        this.state = IDLE;
        this.waitingTimestamp = null;
    }

    public void waitingForUserChoice() {
        this.state = WAITING_FOR_FLOOR_CHOICE;
        this.waitingTimestamp = LocalDateTime.now();
    }

    public void moving() {
        this.state = MOVING;
        this.waitingTimestamp = null;
    }

    public void movingWithUser() {
        this.state = MOVING_WITH_USER;
        this.waitingTimestamp = null;
    }
}
