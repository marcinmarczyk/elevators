package com.tingco.codechallenge.elevator.api;

import com.google.common.collect.Lists;
import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.elevator.event.RequestElevatorEvent;
import com.tingco.codechallenge.elevator.event.SendElevatorWithUserEvent;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

@Service
@Slf4j
class ElevatorControllerImpl implements ElevatorController {

    private final EventBus eventBus;
    private final Executor executor;

    private List<ElevatorThread> elevatorThreads;

    @Value("${com.tingco.elevator.numberofelevators}")
    private int numberOfElevators;

    @Value("${com.tingco.elevator.secondswaiting}")
    private int secondsWaiting;

    @Getter
    private List<Integer> floorRequests = new CopyOnWriteArrayList();

    public ElevatorControllerImpl(EventBus eventBus, @Qualifier("elevatorTaskExecutor") Executor executor) {
        this.eventBus = eventBus;
        this.executor = executor;
    }

    @PostConstruct
    private void init() {
        elevatorThreads = Lists.newArrayList();
        for (int i = 0; i < numberOfElevators; i++) {
            ElevatorThread elevatorThread = new ElevatorThread(ElevatorImpl.of(i));
            elevatorThreads.add(elevatorThread);
            eventBus.register(elevatorThread.getElevator());
        }
        elevatorThreads.forEach(executor::execute);
        eventBus.register(new DeadEventHandler());
    }

    @Override
    public Elevator requestElevator(int toFloor) {
        log.info("Requesting elevatorOpt to floor: {}", toFloor);
        Optional<Elevator> elevatorOpt = findAppropriateElevator(toFloor);
        if (!elevatorOpt.isPresent()) {
            floorRequests.add(toFloor);
            return null;
        }
        val elevator = elevatorOpt.get();
        val elevatorEvent = RequestElevatorEvent.builder().elevatorId(elevator.getId()).floorNum(toFloor).build();
        eventBus.post(elevatorEvent);
        return elevator;
    }

    private Optional<Elevator> findAppropriateElevator(int toFloor) {
        Optional<Elevator> elevatorOpt = findClosestIdle(toFloor);
        if (elevatorOpt.isPresent()) {
            return elevatorOpt;
        }
        return Optional.empty();
    }

    private Optional<Elevator> findClosestIdle(int toFloor) {
        Optional<Elevator> closestElevator = Optional.empty();
        Integer shortestDistance = null;
        List<Elevator> stoppedElevators =
                getElevators().stream().filter(e -> e.getState() == State.IDLE).collect(Collectors.toList());
        for (Elevator e : stoppedElevators) {
            int calculatedDistance = Math.abs(toFloor - e.currentFloor());
            if (shortestDistance == null || shortestDistance > calculatedDistance) {
                shortestDistance = calculatedDistance;
                closestElevator = Optional.of(e);
            }
        }
        return closestElevator;
    }

    @Override
    public List<Elevator> getElevators() {
        return elevatorThreads.stream().map(ElevatorImpl::of).collect(Collectors.toList());
    }

    @Override
    public void releaseElevator(Elevator elevator) {

    }

    @Override
    public void chooseFloor(int elevatorId, int floorNumber) {
        val elevatorEvent = SendElevatorWithUserEvent.builder().elevatorId(elevatorId).floorNum(floorNumber).build();
        eventBus.post(elevatorEvent);
    }

    @Scheduled(fixedRate = 1000)
    public void processQueue() {
        if (!floorRequests.isEmpty()) {
            floorRequests.forEach(f -> requestElevator(floorRequests.remove(0)));
        }
    }

    @Scheduled(fixedRate = 5000)
    public void checkElevatorsWaitingPeriod() {
        getElevators().forEach(e -> {
            if (e.getWaitingTimestamp() != null && e.getWaitingTimestamp().plusSeconds(secondsWaiting).isBefore(LocalDateTime.now())) {
                e.idle();
            }
        });
    }
}
