package com.tingco.codechallenge.elevator

import com.tingco.codechallenge.elevator.api.Elevator
import com.tingco.codechallenge.elevator.api.ElevatorImpl

class MotherOfElevator {
    Elevator anyElevator() {
        ElevatorImpl.of(1)
    }

    Elevator withId(int id) {
        ElevatorImpl.of(id)
    }
}
