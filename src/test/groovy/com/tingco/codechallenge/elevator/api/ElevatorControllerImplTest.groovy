package com.tingco.codechallenge.elevator.api


import com.google.common.eventbus.EventBus
import com.tingco.codechallenge.elevator.MotherOfElevator
import com.tingco.codechallenge.elevator.event.RequestElevatorEvent
import com.tingco.codechallenge.elevator.event.SendElevatorWithUserEvent
import spock.lang.Specification

import java.time.LocalDateTime
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.Executor

class ElevatorControllerImplTest extends Specification {

    def motherOfElevators = new MotherOfElevator()
    def eventBus
    def executor
    def elevatorController

    void setup() {
        eventBus = Mock(EventBus)
        executor = Mock(Executor)
        elevatorController = new ElevatorControllerImpl(eventBus, executor)
    }

    def 'should create required number of thread and register them to event bus'() {
        given:
        def numberOfElevatorsDefined = 5
        elevatorController.numberOfElevators = numberOfElevatorsDefined

        when:
        elevatorController.init()

        then:
        elevatorController.elevatorThreads.size() == numberOfElevatorsDefined
        numberOfElevatorsDefined * eventBus.register(_ as Elevator)
        numberOfElevatorsDefined * executor.execute(_)
        1 * eventBus.register(_ as DeadEventHandler)
    }

    def 'will return existed elevator when requesting one to floor'() {
        given:
        def floorNumber = 5
        def numberOfElevators = 1
        elevatorController.numberOfElevators = numberOfElevators

        when:
        elevatorController.init()
        def result = elevatorController.requestElevator(floorNumber)

        then:
        assert result != null
        1 * eventBus.post(_ as RequestElevatorEvent)
    }

    def 'will return null when requesting elevator to floor and there are no elevators'() {
        given:
        def floorNumber = 5
        def numberOfElevators = 0
        elevatorController.numberOfElevators = numberOfElevators

        when:
        elevatorController.init()
        def result = elevatorController.requestElevator(floorNumber)

        then:
        assert result == null
        0 * eventBus.post(_ as RequestElevatorEvent)
    }

    def 'will not find an elevator if elevatorThreads is empty'() {
        given:
        def floorNumber = 5
        def numberOfElevators = 0
        elevatorController.numberOfElevators = numberOfElevators

        when:
        elevatorController.init()
        def result = elevatorController.findAppropriateElevator(floorNumber)

        then:
        assert result == Optional.empty()
    }

    def 'will find an elevator if elevatorThreads is not empty'() {
        given:
        def floorNumber = 5
        def numberOfElevators = 1
        elevatorController.numberOfElevators = numberOfElevators

        when:
        elevatorController.init()
        def result = elevatorController.findAppropriateElevator(floorNumber)

        then:
        assert result == Optional.of(motherOfElevators.withId(0))
    }

    def 'will find closest, idle elevator if elevatorThreads is not empty'() {
        given:
        def floorNumber = 5
        def numberOfElevators = 2
        elevatorController.numberOfElevators = numberOfElevators

        when:
        elevatorController.init()
        def elevator0 = elevatorController.elevatorThreads[0].elevator
        elevator0.currentFloor = 1
        def elevator1 = elevatorController.elevatorThreads[1].elevator
        elevator1.currentFloor = 2
        def result = elevatorController.findClosestIdle(floorNumber)

        then:
        assert result == Optional.of(elevator1)
    }

    def 'will find closest, idle elevator with id=0, even if another one is closer, but is busy'() {
        given:
        def floorNumber = 5
        def numberOfElevators = 2
        elevatorController.numberOfElevators = numberOfElevators

        when:
        elevatorController.init()
        def elevator0 = elevatorController.elevatorThreads[0].elevator
        elevator0.currentFloor = 1
        elevator0.state = State.IDLE
        def elevator1 = elevatorController.elevatorThreads[1].elevator
        elevator1.currentFloor = 2
        elevator1.state = State.MOVING
        def result = elevatorController.findClosestIdle(floorNumber)

        then:
        assert result == Optional.of(elevator0)
    }

    def 'will find null if elevatorThreads is empty'() {
        given:
        def floorNumber = 5
        def numberOfElevators = 0
        elevatorController.numberOfElevators = numberOfElevators

        when:
        elevatorController.init()
        def result = elevatorController.findClosestIdle(floorNumber)

        then:
        assert result == Optional.empty()
    }

    def 'will return all elevators when getting elevators and elevatorThreads is not empty'() {
        given:
        def numberOfElevators = 3
        elevatorController.numberOfElevators = numberOfElevators
        elevatorController.init()

        when:
        def result = elevatorController.getElevators()

        then:
        assert result.size() == 3
    }

    def 'will return empty list when getting elevators and elevatorThreads is empty'() {
        given:
        def numberOfElevators = 0
        elevatorController.numberOfElevators = numberOfElevators
        elevatorController.init()

        when:
        def result = elevatorController.getElevators()

        then:
        assert result == []
    }

    def 'will post SendElevatorWithUserEvent while choosing floor'() {
        given:
        def anyElevatorId = 0
        def anyFloorNumber = 5

        when:
        elevatorController.chooseFloor(anyElevatorId, anyFloorNumber)

        then:
        1 * eventBus.post(_ as SendElevatorWithUserEvent)
    }

    def 'will not process queue if it is empty'(){
        given:
        elevatorController.floorRequests = []

        when:
        elevatorController.processQueue()

        then:
        0 * eventBus.post(_)
    }

    def 'will process queue if it is not empty'(){
        given:
        def numberOfElevators = 1
        elevatorController.numberOfElevators = numberOfElevators
        elevatorController.init()
        elevatorController.floorRequests = new CopyOnWriteArrayList<>([3])

        when:
        elevatorController.processQueue()

        then:
        1 * eventBus.post(_)
    }

    def 'will not set state to idle if elevator is not waiting longer than 5 seconds'() {
        given:
        def numberOfElevators = 1
        elevatorController.numberOfElevators = numberOfElevators
        elevatorController.init()
        elevatorController.secondsWaiting = 5
        def elevator = elevatorController.elevatorThreads[0].elevator
        elevator.state = State.WAITING_FOR_FLOOR_CHOICE
        elevator.waitingTimestamp = LocalDateTime.now()

        when:
        elevatorController.checkElevatorsWaitingPeriod()

        then:
        elevator.state == State.WAITING_FOR_FLOOR_CHOICE
    }

    def 'will set state to idle if elevator is waiting longer than 1 second'() {
        given:
        def maxSecondsWaiting = 1
        def numberOfElevators = 1
        elevatorController.numberOfElevators = numberOfElevators
        elevatorController.init()
        elevatorController.secondsWaiting = maxSecondsWaiting
        def elevator = elevatorController.elevatorThreads[0].elevator
        elevator.state = State.WAITING_FOR_FLOOR_CHOICE
        elevator.waitingTimestamp = LocalDateTime.now().minusSeconds(maxSecondsWaiting)

        when:
        Thread.sleep(maxSecondsWaiting * 1000 + 500)
        elevatorController.checkElevatorsWaitingPeriod()

        then:
        elevator.state == State.IDLE
    }






}
