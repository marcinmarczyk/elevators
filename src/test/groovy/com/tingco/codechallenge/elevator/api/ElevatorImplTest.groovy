package com.tingco.codechallenge.elevator.api

import com.tingco.codechallenge.elevator.MotherOfElevator
import com.tingco.codechallenge.elevator.event.RequestElevatorEvent
import com.tingco.codechallenge.elevator.event.SendElevatorWithUserEvent
import spock.lang.Specification
import spock.lang.Unroll

import static com.tingco.codechallenge.elevator.api.State.*

class ElevatorImplTest extends Specification {

    def mother = new MotherOfElevator()

    def 'should correctly create Elevator object'() {
        given:
        def id = 1

        when:
        def elevator = mother.anyElevator()

        then:
        assert elevator.id == id
        assert elevator.direction == Elevator.Direction.NONE
        assert elevator.state == IDLE
        assert elevator.waitingTimestamp == null
    }

    def 'should throw NPE when creating ElevatorImpl object with null elevatorThread parameter '() {
        given:
        ElevatorThread elevatorThread = null

        when:
        ElevatorImpl.of(elevatorThread)

        then:
        thrown(NullPointerException)
    }

    def 'won\'t change state if id in RequestElevatorEvent event is not equal to id in object'() {
        given:
        def elevator = mother.anyElevator()
        RequestElevatorEvent event = new RequestElevatorEvent.RequestElevatorEventBuilder().elevatorId(0).build()

        when:
        elevator.getEvent(event)

        then:
        elevator.state == IDLE
    }

    @Unroll
    def 'will change state to #endState if id in RequestElevatorEvent event is equal to id in object and state is #initState'(State initState, State endState) {
        given:
        def elevator = mother.anyElevator()
        elevator.state = initState
        RequestElevatorEvent event = new RequestElevatorEvent.RequestElevatorEventBuilder().elevatorId(1).floorNum(2).build()
        elevator.getEvent(event)

        expect:
        elevator.state == endState

        where:
        initState | endState
        IDLE | MOVING
        WAITING_FOR_FLOOR_CHOICE | MOVING
        MOVING_WITH_USER | MOVING
    }


    def 'won\'t change state if id in SendElevatorWithUserEvent event is not equal to id in object'() {
        given:
        def elevator = mother.anyElevator()
        SendElevatorWithUserEvent event = new SendElevatorWithUserEvent.SendElevatorWithUserEventBuilder().elevatorId(0).build()

        when:
        elevator.getEvent(event)

        then:
        elevator.state == IDLE
    }

    def 'will change state to MOVING if id in SendElevatorWithUserEvent event is equal to id in object'() {
        given:
        def elevator = mother.anyElevator()
        SendElevatorWithUserEvent event = new SendElevatorWithUserEvent.SendElevatorWithUserEventBuilder().elevatorId(1).floorNum(2).build()

        when:
        elevator.getEvent(event)

        then:
        elevator.state == MOVING_WITH_USER
    }

    def 'will increase count of activated buttons'() {
        given:
        def elevator = mother.anyElevator()

        when:
        elevator.moveElevator(2)

        then:
        assert elevator.activatedButtons.size() == 1
        assert elevator.activatedButtons.contains(2)
    }

    def 'will change addressed floor if elevator is not busy'() {
        given:
        def elevator = mother.anyElevator()
        elevator.addressedFloor = 0
        elevator.currentFloor = 0
        elevator.activatedButtons = [6]

        when:
        elevator.moveOneFloor()

        then:
        assert elevator.direction == Elevator.Direction.UP
        assert elevator.addressedFloor == 6
        assert elevator.activatedButtons.isEmpty()
        assert elevator.currentFloor == 1
    }

    def 'will go up one floor when current floor is lower then addressed floor'() {
        given:
        def elevator = mother.anyElevator()
        elevator.addressedFloor = 5
        elevator.currentFloor = 0
        elevator.activatedButtons = [6]

        when:
        elevator.moveOneFloor()

        then:
        assert elevator.direction == Elevator.Direction.UP
        assert elevator.activatedButtons.size() == 1
        assert elevator.currentFloor == 1
    }

    def 'will go down one floor when current floor is greater then addressed floor'() {
        given:
        def elevator = mother.anyElevator()
        elevator.addressedFloor = 5
        elevator.currentFloor = 7
        elevator.activatedButtons = [6]

        when:
        elevator.moveOneFloor()

        then:
        assert elevator.direction == Elevator.Direction.DOWN
        assert elevator.activatedButtons.size() == 1
        assert elevator.currentFloor == 6
    }

    def 'will change state to WAITING_FOR_FLOOR_CHOICE if direction is NONE, current state is MOVING and there are no activated buttons'() {
        given:
        def elevator = mother.anyElevator()
        elevator.addressedFloor = 0
        elevator.currentFloor = 0
        elevator.activatedButtons = []
        elevator.state = State.MOVING

        when:
        elevator.moveOneFloor()

        then:
        assert elevator.state == State.WAITING_FOR_FLOOR_CHOICE
    }

    def 'will change state to IDLE if direction is NONE, current state is MOVING_WITH_USER and there are no activated buttons'() {
        given:
        def elevator = mother.anyElevator()
        elevator.addressedFloor = 0
        elevator.currentFloor = 0
        elevator.activatedButtons = []
        elevator.state = State.MOVING_WITH_USER

        when:
        elevator.moveOneFloor()

        then:
        assert elevator.state == State.IDLE
    }

    def 'will set direction to DOWN when addressed floor is lower than current floor'() {
        given:
        def elevator = mother.anyElevator()
        elevator.addressedFloor = 0
        elevator.currentFloor = 1

        when:
        elevator.specifyDirection()

        then:
        assert elevator.direction == Elevator.Direction.DOWN
    }

    def 'will set direction to UP when addressed floor is greater than current floor'() {
        given:
        def elevator = mother.anyElevator()
        elevator.addressedFloor = 1
        elevator.currentFloor = 0

        when:
        elevator.specifyDirection()

        then:
        assert elevator.direction == Elevator.Direction.UP
    }

    def 'will set direction to NONE when addressed floor is equal to current floor'() {
        given:
        def elevator = mother.anyElevator()
        elevator.addressedFloor = 0
        elevator.currentFloor = 0

        when:
        elevator.specifyDirection()

        then:
        assert elevator.direction == Elevator.Direction.NONE
    }

    def 'will return true if addressed floor is not equal to current floor'() {
        given:
        def elevator = mother.anyElevator()
        elevator.addressedFloor = 0
        elevator.currentFloor = 1

        when:
        def result = elevator.isBusy()

        then:
        assert result
    }

    def 'will return false if addressed floor is equal to current floor'() {
        given:
        def elevator = mother.anyElevator()
        elevator.addressedFloor = 0
        elevator.currentFloor = 0

        when:
        def result = elevator.isBusy()

        then:
        assert !result
    }

    def 'will set waiting timestamp to null and state to IDLE'() {
        given:
        def elevator = mother.anyElevator()
        elevator.state = State.MOVING_WITH_USER

        when:
        elevator.idle()

        then:
        assert elevator.state == State.IDLE
        assert elevator.waitingTimestamp == null
    }

    def 'will set waiting timestamp to current date and time and state to WAITING_FOR_FLOOR_CHOICE'() {
        given:
        def elevator = mother.anyElevator()
        elevator.state = State.IDLE

        when:
        elevator.waitingForUserChoice()

        then:
        assert elevator.state == State.WAITING_FOR_FLOOR_CHOICE
        assert elevator.waitingTimestamp != null
    }

    def 'will set waiting timestamp to null and state to MOVING'() {
        given:
        def elevator = mother.anyElevator()
        elevator.state = State.MOVING_WITH_USER

        when:
        elevator.moving()

        then:
        assert elevator.state == State.MOVING
        assert elevator.waitingTimestamp == null
    }

    def 'will set waiting timestamp to null and state to MOVING_WITH_USER'() {
        given:
        def elevator = mother.anyElevator()
        elevator.state = State.IDLE

        when:
        elevator.movingWithUser()

        then:
        assert elevator.state == State.MOVING_WITH_USER
        assert elevator.waitingTimestamp == null
    }

    def 'will add addressed floor when elevator is moving with user and activated buttons does not contain addressed floor'(){
        given:
        def elevator = mother.anyElevator()
        def state = State.MOVING_WITH_USER
        def activatedButtons = []
        def floor = 5

        when:
        def result = elevator.addAddressedFloorToSortOperation(activatedButtons, state, floor)

        then:
        assert result.size() == 1
        assert result.contains(5)
    }

    def 'will not add addressed floor when elevator is idle and activated buttons does not contain addressed floor'(){
        given:
        def elevator = mother.anyElevator()
        def state = State.IDLE
        def activatedButtons = []
        def floor = 5

        when:
        def result = elevator.addAddressedFloorToSortOperation(activatedButtons, state, floor)

        then:
        assert result.size() == 0
    }

    def 'will not add addressed floor when elevator is moving with user and activated buttons contain addressed floor'(){
        given:
        def elevator = mother.anyElevator()
        def state = State.MOVING_WITH_USER
        def activatedButtons = [5]
        def floor = 5

        when:
        def result = elevator.addAddressedFloorToSortOperation(activatedButtons, state, floor)

        then:
        assert result.size() == 1
    }

    def 'will sort activated buttons when elevator is on 0th floor and going up'(){
        given:
        def elevator = mother.anyElevator()
        elevator.activatedButtons = [8, 5, 7, 1, 4, 3]
        elevator.direction = Elevator.Direction.UP
        elevator.currentFloor = 0
        elevator.addressedFloor = 9

        when:
        elevator.sortActivatedButtons()

        then:
        assert elevator.activatedButtons == [1, 3, 4, 5, 7, 8]
    }

    def 'will sort activated buttons when elevator is on 10th floor and going down'(){
        given:
        def elevator = mother.anyElevator()
        elevator.activatedButtons = [8, 5, 7, 1, 4, 3]
        elevator.direction = Elevator.Direction.DOWN
        elevator.currentFloor = 9
        elevator.addressedFloor = 0

        when:
        elevator.sortActivatedButtons()

        then:
        assert elevator.activatedButtons == [8, 7, 5, 4, 3, 1]
    }

    def 'will sort activated buttons when elevator is on 6th floor and going up'(){
        given:
        def elevator = mother.anyElevator()
        elevator.activatedButtons = [8, 5, 7, 1, 4, 3]
        elevator.direction = Elevator.Direction.UP
        elevator.currentFloor = 6
        elevator.addressedFloor = 9

        when:
        elevator.sortActivatedButtons()

        then:
        assert elevator.activatedButtons == [7, 8, 5, 4, 3, 1]
    }

    def 'will sort activated buttons when elevator is on 6th floor and going down'(){
        given:
        def elevator = mother.anyElevator()
        elevator.activatedButtons = [8, 5, 9, 1, 4, 3]
        elevator.direction = Elevator.Direction.DOWN
        elevator.currentFloor = 6
        elevator.addressedFloor = 0

        when:
        elevator.sortActivatedButtons()

        then:
        assert elevator.activatedButtons == [5, 4, 3, 1, 8, 9]
    }

    def 'will remove activated buttons when elevator is on 6th floor and going down'(){
        given:
        def elevator = mother.anyElevator()
        elevator.activatedButtons = [8, 5, 9, 1, 4, 3]
        elevator.direction = Elevator.Direction.DOWN
        elevator.currentFloor = 6
        elevator.addressedFloor = 0

        when:
        elevator.sortActivatedButtons()

        then:
        assert elevator.activatedButtons == [5, 4, 3, 1, 8, 9]
    }

    def 'will set new addressed floor when elevator is moving with user and activated buttons are not empty'() {
        given:
        def elevator = mother.anyElevator()
        elevator.activatedButtons = [8]
        elevator.currentFloor = 6
        elevator.addressedFloor = 0
        elevator.state = MOVING_WITH_USER

        when:
        elevator.setNewAddressedFloor(elevator.state, elevator.activatedButtons)

        then:
        assert elevator.addressedFloor == 8
        assert elevator.activatedButtons == []
    }

    def 'will not set new addressed floor when elevator is idle and activated buttons are not empty'() {
        given:
        def elevator = mother.anyElevator()
        elevator.activatedButtons = [8]
        elevator.currentFloor = 6
        elevator.addressedFloor = 0
        elevator.state = IDLE

        when:
        elevator.setNewAddressedFloor(elevator.state, elevator.activatedButtons)

        then:
        assert elevator.addressedFloor == 0
        assert elevator.activatedButtons == [8]
    }

    def 'will not set new addressed floor when elevator is moving with user and activated buttons are empty'() {
        given:
        def elevator = mother.anyElevator()
        elevator.activatedButtons = []
        elevator.currentFloor = 6
        elevator.addressedFloor = 0
        elevator.state = MOVING_WITH_USER

        when:
        elevator.setNewAddressedFloor(elevator.state, elevator.activatedButtons)

        then:
        assert elevator.addressedFloor == 0
    }

    def 'will return current floor'() {
        given:
        def elevator = mother.anyElevator()
        elevator.currentFloor = 5

        when:
        def result = elevator.currentFloor()

        then:
        assert result == 5
    }


}
