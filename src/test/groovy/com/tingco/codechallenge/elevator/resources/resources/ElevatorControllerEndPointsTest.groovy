package com.tingco.codechallenge.elevator.resources.resources

import com.tingco.codechallenge.elevator.MotherOfElevator
import com.tingco.codechallenge.elevator.api.ElevatorController
import com.tingco.codechallenge.elevator.resources.ElevatorControllerEndPoints
import com.tingco.codechallenge.elevator.resources.OptionsService
import com.tingco.codechallenge.elevator.resources.dto.OptionsDto
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class ElevatorControllerEndPointsTest extends Specification {
    def motherOfElevators = new MotherOfElevator()
    def elevatorController = Mock(ElevatorController)
    def optionsService = Mock(OptionsService)

    def elevatorControllerEndPoints = new ElevatorControllerEndPoints(elevatorController, optionsService)

    def mockMvc = MockMvcBuilders.standaloneSetup(elevatorControllerEndPoints).build()

    def 'should return pong as a response when requesting "/rest/v1/ping"'() {
        given:
        def url = '/rest/v1/ping'

        when:
        def request = mockMvc.perform(get(url))

        then:
        assert request.andExpect(status().is2xxSuccessful())
        assert request.andExpect(content().string('pong'))
    }

    def 'should request elevator to specified floor when requesting "/rest/v1/requestElevator/"'(){
        given:
        def url = '/rest/v1/requestElevator/5'

        when:
        def request = mockMvc.perform(get(url))

        then:
        assert request.andExpect(status().is2xxSuccessful())
        assert request.andExpect(content().json('''
        {
            'status': 'OK'
        }
        '''))
    }

    def 'should choose floor for specified elevator when requesting "/rest/v1/chooseFloor/"'(){
        given:
        def floorNumber = 5
        def elevatorId = 0
        def url = "/rest/v1/chooseFloor/${elevatorId}/${floorNumber}"

        when:

        def request = mockMvc.perform(get(url))

        then:
        1 * elevatorController.chooseFloor(elevatorId, floorNumber)
        assert request.andExpect(status().is2xxSuccessful())
        assert request.andExpect(content().json('''
        {
            'status': 'OK'
        }
        '''))
    }

    def 'should return options when requesting "/rest/v1/options"'(){
        given:
        def url = '/rest/v1/options'
        def numberOfElevators = 5
        def numberOfFloors = 10

        when:
        1 * optionsService.getOptions() >> new OptionsDto(numberOfElevators, numberOfFloors)
        def request = mockMvc.perform(get(url))

        then:
        assert request.andExpect(status().is2xxSuccessful())
        assert request.andExpect(content().json("""
        {
            'numberOfElevators': ${numberOfElevators},
            'numberOfFloors': ${numberOfFloors}
        }
        """))
    }

    def 'should return elevators when requesting "/rest/v1/currentElevators"'(){
        given:
        def url = '/rest/v1/currentElevators'
        def elevator = motherOfElevators.anyElevator()

        when:
        1 * elevatorController.getElevators() >> [elevator]
        def request = mockMvc.perform(get(url))

        then:
        assert request.andExpect(status().is2xxSuccessful())
        assert request.andExpect(content().json("""
        {
        'elevatorDtos':
            [
                {
                    'id': 1,
                    'floor': 0,
                    'moving': false,
                    'state': 'IDLE',
                    'direction': 'NONE'
                }
            ]
        }
        """))

    }


    def 'should return requests when requesting "/rest/v1/currentRequests"'(){
        given:
        def url = '/rest/v1/currentRequests'

        when:
        1 * elevatorController.getFloorRequests() >> [1, 2, 3]
        def request = mockMvc.perform(get(url))

        then:
        assert request.andExpect(status().is2xxSuccessful())
        assert request.andExpect(content().json("""
        {
        'currentRequests':
            [
                1, 
                2,
                3
            ]
        }
        """))
    }
}
