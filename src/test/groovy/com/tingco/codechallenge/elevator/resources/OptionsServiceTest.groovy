package com.tingco.codechallenge.elevator.resources


import spock.lang.Specification

class OptionsServiceTest extends Specification {

    def optionsService = new OptionsService()

    def 'will properly build OptionsDto'() {
        given:
        def numberOfElevators = 5
        def numberOfFloors = 10
        optionsService.numberOfElevators = numberOfElevators
        optionsService.numberOfFloors = numberOfFloors

        when:
        def result = optionsService.getOptions()

        then:
        assert result.numberOfElevators == numberOfElevators
        assert result.numberOfFloors == numberOfFloors
    }

}
