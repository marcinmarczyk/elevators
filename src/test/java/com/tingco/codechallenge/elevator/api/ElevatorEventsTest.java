package com.tingco.codechallenge.elevator.api;

import com.tingco.codechallenge.elevator.config.ElevatorApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.Callable;

import static com.jayway.awaitility.Awaitility.await;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ElevatorApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class ElevatorEventsTest {
    private final int FLOOR_NUMBER = 2;
    private final int ELEVATOR_ID = 0;

    @Autowired
    private ElevatorController elevatorController;

    @Test
    public void will_check_if_elevator_gets_SendElevatorWithUserEvent(){
        elevatorController.chooseFloor(ELEVATOR_ID, FLOOR_NUMBER);
        await().until(elevatorChangedStateToMovingWithUser());
    }

    @Test
    public void will_check_if_elevator_gets_RequestElevatorEvent(){
        elevatorController.requestElevator(FLOOR_NUMBER);
        await().until(elevatorChangedStateToMoving());
    }

    private Callable<Boolean> elevatorChangedStateToMovingWithUser() {
        return () -> elevatorController.getElevators().get(0).getState() == State.MOVING_WITH_USER;
    }

    private Callable<Boolean> elevatorChangedStateToMoving() {
        return () -> elevatorController.getElevators().get(0).getState() == State.MOVING;
    }

}
